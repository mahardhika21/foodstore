const app = require('../server');
const chai = require('chai');
const request = require('supertest');
const assert = require('assert');
const product_id = null;

describe('Basic Mocha String Test', () => {
    it('should return number of characters is 5', () => {
        assert.equal("Hello".length, 5);
    });
});

describe('Get / product list', () => {
    it('response list product json', (done) => {
        request(app)
            .get('/products')
            .set('Accept', 'application/json')
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })
});

describe('POST / product store', () => {
    it('response with json', (done) => {
        request(app)
            .post('/product')
            .send({ name: 'test', description: 'desc test', price: 1000 })
            .set('Accept', 'application/json')
            .expect(200)
            .end(async (err, res) => {
                console.log(res);
                if (err) return done(err);
                return done();
            });
    });
});
