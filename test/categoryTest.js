const app = require('../server');
const chai = require('chai');
const request = require('supertest');
const assert = require('assert');
const category_id = null;


describe('Get / category list', () => {
    it('respone with json', (done) => {
        request(app)
            .post('/category')
            .set('Accept', 'application/json')
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                return done();
            });
    });
});

describe('POST / category store', () => {
    it('response with json', (done) => {
        request(app)
            .post('/category')
            .send({ name: 'test'})
            .set('Accept', 'application/json')
            .expect(200)
            .end(async (err, res) => {
               // console.log(res);
                if (err) return done(err);
                return done();
            });
    });
});