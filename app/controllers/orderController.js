const orderModel = require('../models/order');
const orderItemModel = require('../models/orderItem');
const decode = require('../libraries/decodedJWT');
const cartItemModel = require('../models/cartItem');
const deliveryModel = require('../models/deliveryAddress');
const logging = require('../libraries/logging');
const invoiceModel = require('../models/invoice');
const mongoose = require('mongoose');

async function index(req, res) {
    try {
        let { limit = 10, skip = 0 } = req.query;
        const user = decode(rq.headers.authorization).payload;
        let orderData = await orderModel.find({ user: user.id }).skip(parseInt(skip)).limit(parseInt(limit)).populate('order_items').sort('-createdAt');
        let countData = await orderModel.find({ user: user.id }).countDocuments();

        return res.json({
            success: true,
            message: 'success get data',
            data: orderData.map(order => order.toJSON({ virtuals : true })),
            countData: countDatas
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            "success": "false",
            "message": "failed get location",
            "log": err.message,
        });
    }
}

async function store(req, res) {
    try {
        const user = decode(req.headers.authorization).payload;
        const payload = req.body;
        let items = await cartItemModel.find({ user: user.id })
            .populate('product');
         let address = await deliveryModel.find({ _id: payload.delivery_address });
        if (!items.length || address) {
            return res.json({ success: true, message: 'Can not create order because you have no items incart' });
        }

        let order = new orderModel({
            _id: mongoose.Types.ObjectId(),
            status: 'waiting_payment',
            delivery_fee,
            delivery_address: {
                provinsi: address.provinsi,
                kabupaten: address.kabupaten,
                kecamatan: address.kecamatan,
                kelurahan: address.kelurahan,
                detail: address.detail
            },
            user: user.id,
        });
        let orderItems = await orderItemModel.insertMany(
                items.map(item => ({...item, 
                        name: item.product.name,
                        qty: parseInt(item.qty), 
                        price: parseInt(item.product.price),
                        order: order._id,
                        product: item.product._id
                })
            ));
        orderItems.forEach(item => order.order_items.push(item));
        order.save();

        await cartItemModel.deleteMany({ user: user.id });
        return res.json(order);

        
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            "success": "false",
            "message": "failed get location",
            "log": err.message,
        });
    }
}

async function invoice(req, res) {
    try {
        const user = decode(req.headers.authorization).payload;
        let { skip = 0, limit = 10 } = req.query;
        let invoiceData = await invoiceModel.find({ user: user.id }).skip(parseint(skip)).limit(parent(limit));
        let countInvoice = await invoiceModel.find({ user: user.id }).countDocuments();

        return res.json({
            success: true,
            message: 'success get data',
            data: invoiceData,
            countdata: countInvoice
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            "success": "false",
            "message": "failed get location",
            "log": err.message,
        });
    }
}

module.exports = {
    index, store , invoice
}