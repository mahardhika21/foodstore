const productModel = require('../models/product');
const config = require('../config/config');
const decode = require('../libraries/decodedJWT');
const categoryModel = require('../models/category');
const logging = require('../libraries/logging');
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const { count } = require('console');

async function index(req, res) {
    try {
        let { limit, skip, search = '', category = '' } = req.query;

        let criteria = {
            name: { $regex: search }
        };
        if (category.length > 1) {
            q = await categoryModel.findOne({ name: { $regex: category } });
            if (q != undefined) {
                criteria = { ...criteria,  category: q._id }
            }
        }
        let count = await productModel.find(criteria).countDocuments();
        let productData = await productModel.find(criteria).limit(parseInt(limit)).skip(parseInt(skip)).populate('category').populate('tags');

        return res.json({
            success: true,
            data: productData,
            count : count,
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });       
    }
}

async function store(req, res) {
    try {
        const payload = req.body;
        const image =  await uploadFile(req.files.image[0]);
        
        await productModel.create({
            name: payload.name,
            description: payload.description,
            price: payload.price,
            image_url: image.name,
            category: payload.category,
            tags : payload.tags,
            active: 1
        });

        return res.json({
            success: true,
            message: 'success store data'
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: "false",
            message: "err failed",
            log: err.message
        });       
    }
}

async function update(req, res) {
    try {
        const payload = req.body;
        const productData = await productModel.findOne({ _id: req.params.id });
        
        if (productData == undefined) {
            return res.json({
                success: false,
                message: 'product not found'
            });
        }

        const image = req.files.image != undefined ? await uploadFile(req.files.image[0]) : { status : false };
        let img_url = productData.image_url;
        if (image.status) {
            img_url = image.name;
             if(fs.existsSync(productData.image_url)) {
                    fs.unlink(productData.image_url, () => { return true });
            }
        }

        await productModel.updateOne({
            _id : productData._id
        },{
            name: payload.name,
            description: payload.description,
            price: payload.price,
            image_url: img_url,
            category: payload.category == undefined ? productData.category : payload.category,
            tags : payload.tags == undefined ? productData.tags : payload.tags,
        });
        
        return res.json({
            success: true,
            message: 'success update data'
        });

    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: "false",
            message: "err failed",
            log: err.message
        });   
    }
}

async function uploadFile(file)
{
    if (file) {
        let filename = file.originalname;
        let fileType = filename.split(".").length == 2 ? filename.split(".")[1] : "jpg";
        let newname = Date.parse(new Date()) + '-' + Math.floor(Math.random() * 999) + '.' + fileType;
        const target = path.join(__dirname, config.ASSETS + "/", newname)
        // const target = path.join(__dirname, config.ASSETS + "/accounts/premium/", file.originalname)
        fs.renameSync(file.path, target);
        promisify(fs.unlink);
        return {
            status: true,
            name : 'public/img/'+newname,
        };
    }else {
        return {
            status: false,
            name: null,
        };
    }
}

async function detail(req, res) {
    try {
        const productData = await productModel.findOne({ _id: req.params.id });

        return res.json({
            success: true,
            message: 'success get data product',
            data: productData,
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: err.message,
        });
    }
}

async function destroy(req, res) {
    try {
        const productData = await productModel.findOne({ _id: req.params.id });
        if (productData == undefined) {
            return res.json({ success: false, message: 'product not found' });
        }

        await productModel.deleteOne({
            _id: productData._id,
        }).then((data) => {
            if(fs.existsSync(productData.image_url)) {
                    fs.unlink(productData.image_url, () => { return true });
            }   
        }).finally(() => {
            return res.json({ success: true, message: 'success delete data' });
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: err.message
        });
    }
}

module.exports = {
    index, store, update, detail, destroy
}