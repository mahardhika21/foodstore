const cartModel = require('../models/cartItem');
const decode = require('../libraries/decodedJWT');
const looging = require('../libraries/logging');
const productModel = require('../models/product');

async function index(req, res) {
    try {
        const user = decode(req.headers.authorization).payload;
        const cartData = await cartModel.find({
            user: user.id
        });

        return res.json({ success: true, message: 'success get data', data: cartData });
    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: false,
                message: "err failed",
                log: err.message
        });      
    }
}

async function store(req, res) {
    try {

    } catch (err) {
         looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: false,
                message: "err failed",
                log: err.message
        });      
    }
}

async function update(req, res) {
    try {
        const { items } = req.body;
        const productIds = items.map(itm => itm._id);

        const products = await productModel.find({ _id: { $in: productIds } });

        let cartItems = items.map(item => {
        let relatedProduct = products.find(product =>
             product._id.toString() === item._id);
        return {
            _id: relatedProduct._id, 
            product: relatedProduct._id, 
            price: relatedProduct.price, 
            image_url: relatedProduct.image_url, 
            name: relatedProduct.name, 
            user: req.user._id, 
            qty: item.qty
            }
        });

        await cartModel.bulkWrite(cartItems.map(item => {
        return {
        updateOne: {
        filter: {user: req.user._id,
        product: item.product}, 
        update: item, 
        upsert: true
        }
        }
        }));

return res.json(cartItems);
    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: false,
                message: "err failed",
                log: err.message
        }); 
    }
}

module.exports = { index, store, update }