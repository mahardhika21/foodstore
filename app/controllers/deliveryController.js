const deliveryModel = require('../models/deliveryAddress');
const logging = require('../libraries/logging');
const decode = require('../libraries/decodedJWT');

async function index(req, res) {
    try {
        const user = decode(req.headers.authorization).payload;

        return res.json({
            success: true,
            message: 'success get data',
            data: await deliveryModel.find({
                user: user.id,
            }),
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });      
    }
}

async function store(req, res) {
    try {
        const payload = req.body;
        const user = decode(req.headers.authorization).payload;
        let deliver = await deliveryModel.create({
            name: payload.name,
            kelurahan: payload.kelurahan,
            kecamatan: payload.kecamatan,
            kabupaten: payload.kabupaten,
            provinsi: payload.provinsi,
            detail: payload.detail,
            user: user.id
        });
        return res.json({
            success: true,
            message: 'succes store data',
            id: deliver._id,
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });     
    }
}

async function show(req, res) {
    try {
        const user = decode(req.headers.authorization).payload;
        const deliveryData = await deliveryModel.findOne({
              _id: req.params.id,
              user: user.id
          });
    
        if (deliveryData == undefined) {
              return res.json({ success: false, message: 'data not found' });
        }
        return res.json({
            success: true,
            message: 'success get data',
            data: deliveryData,
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });      
    }
}

async function update(req, res) {
    try {
        const payload = req.body;
        const user = decode(req.headers.authorization).payload;
        const deliveryData = await deliveryModel.findOne({
              _id: req.params.id,
              user: user.id
          });
    
        if (deliveryData == undefined) {
              return res.json({ success: true, message: 'data not found' });
        }
        let deliver = await deliveryModel.updateOne({
            _id: deliveryData._id,
        },{
            name: payload.name,
            kelurahan: payload.kelurahan,
            kecamatan: payload.kecamatan,
            kabupaten: payload.kabupaten,
            provinsi: payload.provinsi,
            detail: payload.detail,
            user: user.id
        });
        return res.json({
            success: true,
            message: 'succes update data',
            id: deliver._id,
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });     
    }
}

async function destroy(req, res) {
      try {
          const user = decode(req.headers.authorization).payload;
          const deliveryData = await deliveryModel.findOne({
              _id: req.params.id,
              user: user.id
          });
    
          if (deliveryData == undefined) {
              return res.json({ success: true, message: 'data not found' });
          }
          await deliveryModel.deleteOne({
              _id: deliveryData._id,
          });

        return res.json({
            success: true,
            message: 'success delete data',
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: false,
                message: "err failed",
                log: err.message
        });      
    }
}

module.exports = {
    index, store, update, destroy, show
}
