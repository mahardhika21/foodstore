let Product = require('../models/product');
let Category = require('../models/productCategory');
let Extra = require('../models/extra');
let Discount = require('../models/discount');
let Shiping = require('../models/shipping');
let Schedule = require('../models/schedule');
let Voucher = require('../models/voucher');
let voucherMember = require('../models/voucherMember');

async function index(req, res, next) {
    try {  
        if (req.params.type == 'product') {
                await   Product.insertMany([{
                        name: "Indomi Goreng Original",
                        image: "https://m.media-amazon.com/images/I/71B4YNvwCtL._SL1500_.jpg",
                        price: 2800,
                        group_id: "6113899a69eb04a52ad4319d",
                        category_id: "61137da169eb04a52ad4319a",
                        barcode: "https://www.incimages.com/uploaded_files/image/1920x1080/*Barcode_32896.jpg",
                        subcat: ["mie", "mie goreng", "indofood"],
                        descript: "<p>produt indomi goreng mantap</p>",
                        note: "<p>catatan produk</p>",
                        image_list: ["https://m.media-amazon.com/images/I/71B4YNvwCtL._SL1500_.jpg", "https://m.media-amazon.com/images/I/71B4YNvwCtL._SL1500_.jpg"],
                        price_list: [
                            {
                                name: "harga reseler 1",
                                min: 1,
                                price: 2800,
                                active: 1
                            },
                            {
                                name: "harga reseler 2",
                                min: 24,
                                price: 2500,
                                active: 1
                            },
                            {
                                name: "harga reseler 3",
                                min: 50,
                                price: 2100,
                                active: 1
                            }
                        ],
                        price_detail: {
                            netto: 2800
                        },
                        status: 1,
                        active: 1,
                        
                    }, {
                        name: "Kacang Kulit Garuda 260 gr",
                        image: "https://s1.bukalapak.com/img/1062039893/large/GARUDA_KACANG_KULIT_180G.jpg",
                        price: 7000,
                        group_id: "6113899a69eb04a52ad4319d",
                        category_id: "61137da169eb04a52ad4319a",
                        barcode: "https://www.incimages.com/uploaded_files/image/1920x1080/*Barcode_32896.jpg",
                        subcat: ["kacang", "kacang garud", "kcang kulit", "indofood"],
                        descript: "<p>produt makanan ringan indofood</p>",
                        note: "<p>catatan produk</p>",
                        image_list: ["https://s1.bukalapak.com/img/1062039893/large/GARUDA_KACANG_KULIT_180G.jpg", "https://s1.bukalapak.com/img/1062039893/large/GARUDA_KACANG_KULIT_180G.jpg"],
                        price_list: [
                            {
                                name: "harga reseler 1",
                                min: 1,
                                price: 7000,
                                active: 1
                            },
                            {
                                name: "harga reseler 2",
                                min: 10,
                                price: 6200,
                                active: 1
                            }
                        ],
                        price_detail: {
                            netto: 7000
                        },
                        status: 1,
                        active: 1,
                    }, {
                        name: "Susu bear brand",
                        image: "https://my-test-11.slatic.net/p/c334711a5ee6c7ed9d648007908aa100.jpg",
                        price: 8500,
                        group_id: "6113899a69eb04a52ad4319d",
                        category_id: "61139f2c69eb04a52ad431a4",
                        barcode: "https://www.incimages.com/uploaded_files/image/1920x1080/*Barcode_32896.jpg",
                        subcat: ["susu", "beard brand", "susu beruang", "beruang"],
                        descript: "<p>produt minuman susu</p>",
                        note: "<p>catatan produk</p>",
                        image_list: ["https://my-test-11.slatic.net/p/c334711a5ee6c7ed9d648007908aa100.jpg", "https://my-test-11.slatic.net/p/c334711a5ee6c7ed9d648007908aa100.jpg"],
                        price_list: [
                            {
                                name: "harga reseler 1",
                                min: 1,
                                price: 8500,
                                active: 1
                            },
                            {
                                name: "harga reseler 2",
                                min: 24,
                                price: 7000,
                                active: 1
                            }
                        ],
                        price_detail: {
                            netto: 8500
                        },
                        status: 1,
                        active: 1,
            }]).then((data) => {
                return  res.json({
                    success: "true",
                    message: "success",
                    data : data
                })
            }).catch((err) => {
                return  res.json({
                    success: "false",
                    message: "false",
                    log : err.stack
                })
            });
        } else if (req.params.type == 'shiping') {
            Shiping.insertMany([
                {
                    title: "Biaya Ongkir Sesuai jadwal",
                    type: 1,
                    ship_price_list: [{
                        number : 1,
                        price: 0,
                        buy_stat: 1,
                        radius_stat : 1,
                        max_trx: 250000,
                        min_trx: 250000,
                        max_radius: 5,
                        min_radius : 1,
                    },{
                        number : 2,
                        price: 1000,
                        buy_stat: 1,
                        radius_stat : 1,
                        max_trx: 249999,
                        min_trx: 0,
                        max_radius: 20,
                        min_radius : 1,
                    },{
                        number : 3,
                        price: 2000,
                        buy_stat: 0,
                        radius_stat : 1,
                        max_trx: 249999,
                        min_trx: 0,
                        max_radius: 50,
                        min_radius : 20,
                    }],
                    default_price: 3000,
                    active : 1,
                }, {
                    title: "Biaya Ongkir tidak Sesuai jadwal",
                    type: 0,
                    ship_price_list: [{
                        number : 1,
                        price: 2000,
                        buy_stat: 1,
                        radius_stat : 1,
                        max_trx: 110000000,
                        min_trx: 250000,
                        max_radius: 20,
                        min_radius : 1,
                    },{
                        number : 2,
                        price: 3000,
                        buy_stat: 1,
                        radius_stat : 1,
                        max_trx: 249999,
                        min_trx: 0,
                        max_radius: 30,
                        min_radius : 1,
                    }],
                    default_price: 5000,
                    active : 1,
                }
            ]).then((data) => {
                return  res.json({
                    success: "true",
                    message: "success",
                    data : data
                })
            }).catch((err) => {
                return  res.json({
                    success: "false",
                    message: "false",
                    log : err.stack
                })
            });
        } else if (req.params.type == 'promo') {
            Discount.insertMany([{
                name: "promo Seru",
                type: "nominal",
                image: "",
                presentase_price: "price",
                minimal_nominal: 0,
                discount_nominal: 1000,
                start_date: new Date('2021-08-17'),
                end_date : new Date('2021-08-22')
            }]).then((data) => {
                return  res.json({
                    success: "true",
                    message: "success",
                    data : data
                })
            }).catch((err) => {
                return  res.json({
                    success: "false",
                    message: "false",
                    log : err.stack
                })
            });
        } else if (req.params.type == 'jadwal') {
            Schedule.insertMany([{
                day: 1,
                type: 'work',
                hodyday_stat: 0,
                work_time_start: '08:00',
                 work_time_end : '17:00',
                active : 1,
            },
            {
                day: 2,
                type: 'work',
                hodyday_stat: 0,
                work_time_start: '08:00',
                 work_time_end : '17:00',
                active : 1,
                },
            {
                day: 3,
                type: 'work',
                hodyday_stat: 0,
                work_time_start: '08:00',
                 work_time_end : '17:00',
                active : 1,
                },
            {
                day: 4,
                type: 'work',
                hodyday_stat: 0,
                work_time_start: '08:00',
                 work_time_end : '17:00',
                active : 1,
                },
            {
                day: 6,
                type: 'work',
                hodyday_stat: 0,
                work_time_start: '08:00',
                 work_time_end : '17:00',
                active : 1,
                },
            {
                day: 7,
                type: 'holyday',
                hodyday_stat: 0,
                work_time_start: '08:00',
                work_time_end : '17:00',
                active : 1,
            }
            ]).then((data) => {
                 return  res.json({
                    success: "true",
                    message: "success",
                    data : data
                })
            }).catch((err) => {
                 return  res.json({
                    success: "false",
                    message: "false",
                    log : err.stack
                })
            })
        } else if (req.params.type == 'voucher') {
            Voucher.insertMany([{
                name: "vocher member baru",
                code : "xytre",
                type: "ship",
                dis_type: "nominal",
                ship_type: "nominal",
                image: "",
                ship_nominal: 5000,
                active: 1,
                lifetime_stat : 1,
            }, {
                name: "vocher ppkm baru",
                code : "yupar",
                type: "discount",
                dis_type: "nominal",
                ship_type: "nominal",
                image: "",
                ship_nominal: 15000,
                active: 1,
                lifetime_stat: 0,
                start_date: new Date('2021-09-01 23:59:00'),
                end_date : new Date('2021-09-20 23:59:00')
                }, {
                name: "vocher ramadan baru",
                type: "discount",
                code : "teap",
                dis_type: "presentase",
                ship_type: "nominal",
                image: "",
                ship_nominal: 10,
                active: 1,
                lifetime_stat: 0,
                start_date: new Date('2021-09-03 23:59:00'),
                end_date : new Date('2021-09-20 23:59:00')
            }
            ]).then((data) => {
                if (data != undefined) {
                    voucherMember.create({
                        voucher_id : data[0]._id,
                        member_id: '610a98f21aaa8a0aad2d770e',
                        total: 1,
                        log_use: {
                            trx_list : [],
                        }
                    }).then((data) => {
                        return res.json({ success: "true", message: "success insert data voucer" });
                    })        
                }
            });
        } else if (req.params.type == 'member') {
            
        }
    } catch (err) {
        return res.json({
            success: "false",
            message: "failed",
            log : err.stack
        })
    }
}

async function storeProduct() {
   
}

module.exports = {
    index
}