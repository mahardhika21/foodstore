const tagsModel = require('../models/tag');
const decode = require('../libraries/decodedJWT');
const looging = require('../libraries/logging');

async function index(req, res) {
    try {
        return res.json({
            success: true,
            message: "success get data",
            data: await tagsModel.find({
               name : { $regex : req.query.name == undefined ? '' : req.query.name }
            }),
        });
    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });      
    }
}

async function store(req, res) {
    try {
        const payload = req.body;
        
        await tagsModel.create({
            name: payload.name,
        });
        return res.json({
                success: true,
                message: 'success insert data',
        });
    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });       
    }
}

async function show(req, res) {
    try {
        return res.json({
            success: true,
            message: "success get data",
            data: await tagsModel.findOne({ _id : req.params.id }),
        });
    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });
    }
}

async function update(req, res) {
    try {
        tagData = await tagsModel.findOne({ _id: req.params.id });

        if (tagData == undefined) {
            return res.json({ success: false, message: 'category not found' });
        }

        await tagsModel.updateOne({
            _id: tagData._id,
        }, {
            name: req.body.name,
        }).then((data) => {
            return res.json({
                success: true,
                message: 'success update data',
            });
        });

    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });
    }
}

async function destroy(req, res) {
    try {

        await tagsModel.deleteOne({ _id: req.params.id }).then((data) => {
            if (data) {
                return res.json({ success: true, message: 'success delete data' });
            } else {
                return res.json({ success: false, message: 'failed delete data' });
            }
        });

    } catch (err) {
        looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
                success: "false",
                message: "err failed",
                log: err.message
        });
    }
}

module.exports = {
    index, store, show, update, destroy
}