let userModel = require('../models/user');
let jwt = require('jsonwebtoken');
let config = require('../config/config');
let crypto = require('crypto');
let axios = require('axios');
const decode = require('../libraries/decodedJWT');
const logging = require('../libraries/logging');
const os = require('os');

async function login(req, res, next) {
    try {
        let payload = req.body;
        let query = {
                password: crypto.createHash('sha256').update(payload.password).digest('hex'),
                email: payload.email,
                active: 1,
        };

        let loginData = await userModel.findOne(query);

        if (loginData == undefined) {
            return res.json({
                    success: "false",
                    message: "email or password wrong",
                });
        }

         let data = {
            id: loginData._id,
            type: loginData.role,
            hostname : os.hostname()
         };
        
         return res.json({
            success: "true",
            message: "success get token access",
            token : jwt.sign(data, config.JWT_SECRET),
        })

    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: "false",
            message: "failed : err",
            log : err.message
        })
    }
}

async function updatePassword(req, res) {
    let user = decode(req.headers.authorization).payload;
    try {
        let payload = req.body;
        let query = {
                password: crypto.createHash('sha256').update(payload.password_old).digest('hex'),
                _id: user.id,
                active : 1,
        };

        loginData = await userModel.findOne(query);
         if (loginData == undefined) {
            return res.json({
                success: "false",
                message: "password wrong",
            });
        }
        await Member.updateOne({ _id: user.id },
                    { password: crypto.createHash('sha256').update(payload.password_new).digest('hex') });
        return res.json({
                success: "true",
                message: "success update password",
        });

    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
         return res.json({
            success: "false",
            message: "failed : err",
            log : err.message
        })
    }
}

async function test(req, res, next)
{
    try {
   
        return res.json({
            pwd: crypto.createHash('sha256').update('member123').digest('hex'),
            data: "ok",
            os: os.hostname(),
            os_all : os.platform(),
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: "false",
            message: "falied get token access",
            log : err.message,
        });
    }
}

async function location(req, res, next) {
    var jsonData, baseUrl;
    try {
        if (req.query.type == 'kabupaten') {
            baseUrl = `http://admin.hemofiliakita.org/api/kabupaten/${req.query.id}`;
        } else if (req.query.type == 'kecamatan') {
            baseUrl = `http://admin.hemofiliakita.org/api/kecamatan/${req.query.id}`;
        } else if (req.query.type == 'kelurahan') {
            baseUrl =  `http://admin.hemofiliakita.org/api/kelurahan/${req.query.id}`;  
        }else {
            baseUrl = "http://admin.hemofiliakita.org/api/provinsi";
        }

       let data = await axios.get(baseUrl);
       return res.json(
             data.data 
        );
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            "success": "false",
            "message": "failed get location",
            "log": err.message,
        });
    }
}

async function register(req, res) {
    try {
        const payload = req.body;
        await userModel.create({
            name: payload.name,
            password: crypto.createHash('sha256').update(payload.password).digest('hex'),
            email: payload.email,
            active: 1,
            role : 'member'
        }).then((data) => {
            return res.json({ success: true, message: 'register success' });
        });

    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            "success": false,
            "message": "failed get location",
            "log": err.message,
        });
    }
}



module.exports = {
    test,
    login,
    updatePassword,
    location,
    register
}
