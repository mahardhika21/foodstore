let mongoose = require('mongoose');
const { model, Schema } = mongoose;

let userSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        unique: {
            value: true,
            message : "email can't duplicate"
        }
    },
    role: {
        type: String,
        enum: ['superadmin', 'admin', 'member'],
        required: true,
    },
    password: {
        type: String,
        required : true,
    },
    active: {
        type: Number,
        enum:[1, 0]
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
}, { collection : 'users' });

module.exports = model('User', userSchema);