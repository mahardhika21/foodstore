const mongoose = require('mongoose');
const { model, Schema } = mongoose;

const articleSchema = Schema({
    title: {
        type: String,
    },
    body: {
        type: String
    }
}, { timestamps: true });

module.exports = model('Article', articleSchema);