let mongoose = require('mongoose');
const { model, Schema } = mongoose;
const mongoosePaginate = require('mongoose-paginate-v2');

let productSchema = Schema({
    name: {
        type: String,
        minlength : [3, 'Panjang nama Minimal 3 karakter'],
        required: [true, 'nama product harus di isi'],
    },
    description: {
        type: String,
        maxlength : [1000, 'Panjang Karakter maximal 1000 karakter'],
    },
    price: {
        type: Number,
        default : 0,
    },
    image_url: {
        type : String  
    },
    active: {
        type: Number,
        enum: [1, 0],
        default : 1,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref : 'Category',
    },
    tags: [
        {
            type: Schema.Types.ObjectId,
            ref : 'Tag'
        }  
    ],
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
}, { collection: 'product' });

module.exports = model('Product', productSchema);