const mongoose = require('mongoose');
const shcema = require('../graphql/schema');
const { model, Schema } = mongoose;

cartItemSchema = Schema({
    name: {
        type: String,
        required : [true, 'nama makanan tidak boleh dikosongkan'],
    },
    qty: {
        type: Number,
    },
    price: {
        type: Number,
        default : 0
    },
    image_url: {
        type : String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    product: {
        type: Schema.Types.ObjectId,
        ref : 'product'
    },

}, { collection: 'cart_item' });

module.exports = model('CartItem', cartItemSchema);