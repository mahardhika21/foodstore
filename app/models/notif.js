let mongoose = require('mongoose');
const { model, Schema } = mongoose;

let notifSchema = Schema({
    title: {
        type: String,
        required: [true, 'title cant by null or empty']
    },
    content: {
        type: String,
        required: [true, "content can'nt by null or empty"],
    },
    user_type : {
        type : String,
        enum : ['member', 'doctor'],
        default : 'member'
    },
    type : {
        type: String,
        enum: ['broadcast ', 'private'],
        default : 'member'
    },
    content: {
        type: String,
        required: [true, "content can'nt by null or empty"],
    },
    read: {
        type: Number,
        default:0,
        required: true,
    },
    active: {
        type: Number,
        enum : [1, 0],
        required : true,
       
    },
    member_id : {
        type: mongoose.Types.ObjectId, /*Member*/
        ref: "Member",
        required: true 
    },
    doctor_id : {
        type: mongoose.Types.ObjectId, /*Member*/
        ref: "Doctor",
        required: true 
    },
    created_at: {
        type: Date,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = model('notif', notifSchema);