const mongoose = require('mongoose');
const { model, Schema } = mongoose;

const deliveryAddressSchema = Schema({
    name: {
        type: String,
        required: [true, 'nama tidak boleh kosong'],
        maxLength : [255, 'maximal karakter 255'],
    },
    kelurahan: {
        type: String,
        required: [true, 'kelurahan tidak boleh diksoongkan'],
    },
    kecamatan: {
        type: String,
        required: [true, 'kecamatan tidak boleh diksoongkan'],
    },
    kabupaten: {
        type: String,
        required: [true, 'kabupaten tidak boleh diksoongkan'],
    },
    provinsi: {
        type: String,
        required: [true, 'provinsi tidak boleh diksoongkan'],
    },
    detail: {
        type: String,
        required : [true, 'detail alamat harus disi']
    },
    user: {
        type: Schema.Types.ObjectId,
        required : [true, 'user harus di isi']
    },
}, { collection: 'delivery_address', timestamps : true });

module.exports = model('DeliveryAddress', deliveryAddressSchema);