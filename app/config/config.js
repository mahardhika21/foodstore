const dotenv = require('dotenv').config({ encoding: 'latin1' });
// console.log(dotenv.PORT);
module.exports = {
    PORT: process.env.PORT,
    HTTPS: process.env.HTTPS_STAT, //
    JWT_SECRET: process.env.JWT_SECRET,
    ASSETS: "../../public/img/",
    ASSETS_FILE: "../../public/file/",
    MONGODB_URI: process.env.MONGODB_URI,
    FCM_SERVERKEY: process.env.FCM_SERVERKEY,
    PUBLIC_DIR: process.env.PUBLIC_DIR,
    FCM_SERVERKEY: process.env.FCM_SERVERKEY,
    MODE: process.env.MODE,
    MAILER_MASTER: process.env.MAILER_MASTER,
    MAILER_PWD: process.env.MAILER_PWD,
    URL_WEB: process.env.HTTPS_STAT == 'off' ? process.env.URL_DEV : process.env.URL_LIVE,
    PREVIEW_STAT: true,
    REDIS: {
        _host: "127.0.0.1",
        _port: "6379",
        _pass: ""
    },
}
