const { buildSchema } = require('graphql');


const shcema1 = buildSchema(
  `
  type ArticleNew {
     _id: ID!
      title: String!
      body: String!
      createdAt: String!
      message : String
  }

  type Respdata {
    success: String!
  }
  type Query {
        message: String
        message1 : String
        article : [ArticleNew!],
        test (id : Int) : Int,
        test_data : Respdata
    }
    `
);

const shcema = buildSchema(`
  type Article {
      _id: ID!
      title: String!
      body: String!
      createdAt: String!
      message : String
    }

  type Resdata {
      success : String!
      message : String!
  }

  input ArticleInput {
    title: String!
    body: String!
  }

  input AricleUpdate {
    title : String!
    body : String!
    id : String!
  }

  type Query {
    articles:[Article!]
    detailArticle(id : String): Article!
  }

  type Mutation {
    createArticle(article:ArticleInput): Article
    updateArticle(article:AricleUpdate): Resdata
    deleteArticle(id:String) : Resdata 
  }

  schema {
    query: Query
    mutation: Mutation
  }

`);

module.exports = shcema;