const Article = require('../../models/article');

const root1 = {
    message: () => 'hello world',
    message1: () => 'message 1 nihh',
    article: async () => {
        try {
            const articleData = await Article.find();

            return articleData.map((data) => {
                return {
                ...data._doc,
                _id: data.id,
                createdAt: new Date(data._doc.createdAt).toISOString(),
            }
            });
        } catch (err) {
            throw err;
        }
    },
    test: ({ id }) => {
        return id;
    },
    test_data: () => {
        return { ...{}, success : 'ok' }
    }
};

const root = {
   articles: async (req, res) => {
        try {
        const articlesFetched = await Article.find()
        return articlesFetched.map(article => {
            return {
            ...article._doc,
            _id: article.id,
            createdAt: new Date(article._doc.createdAt).toISOString(),
            }
        })
        } catch (error) {
        throw error
        }
  },

  createArticle: async args => {
    try {
        const { title, body } = args.article
        const article = new Article({
            title,
            body,
        })
        const newArticle = await article.save()
        return { ...newArticle._doc, _id: newArticle.id }
        } catch (error) {
        throw error
        }
    },
    updateArticle: async (args) => {
        try {
         const { title, body, id } = args.article;
           await Article.updateOne({
                _id: id
            }, {
                title: title,
                body: body
            });
            return { ...{}, success : 'ok', message : 'success updat data',  }
        } catch (err) {
            throw error;
      }
    },

    deleteArticle: async ({ id }) => {
        try {
            await Article.deleteOne({
            _id: id
            });
            return {...{}, success : 'ok', message : 'success delete data'}
        } catch (err) {
            throw err;
        }
    },
    detailArticle: async ({ id }) => {
        try {
            const data = await Article.findOne({ _id: id });
            return { ...{}, title: data.title, body: data.body };
        } catch (err) {
            throw err;
        }  
    },
    testGraphql: async () => {
        try {
            return {
                success : ok
            }
        } catch (err) {
            throw err
        }
    }
    
}

module.exports = root;