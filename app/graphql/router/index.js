const expressGraphQL = require('express-graphql').graphqlHTTP;
const graphqlSchema = require('../schema/index');
const graphqlResolver = require('../resolvers/index');
express = require('express');
app = express()

app.use('/graphql', expressGraphQL({
    schema: graphqlSchema,
    rootValue: graphqlResolver,
    graphiql : true,
}));


module.exports = app