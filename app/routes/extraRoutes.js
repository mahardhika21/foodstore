let express = require('express');
let router = express.Router();
let authApp = require('../middleware/authMiddleware');
const multer = require('multer');
const imgFilter = require('../middleware/imageFilter');
const uploadFile = multer({ dest: 'public/tmp/' }).fields([{ name: 'xlsx', maxCount: 1 }]);
const upload = multer({ dest: 'public/tmp/', fileFilter: imgFilter.imageFIlter });
const uploadData = upload.fields([{ name: 'picture', maxCount: 1000 }]);
const config = require('../config/config');
// service



module.exports = router;


