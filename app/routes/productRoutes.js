const express = require('express');
const router = express.Router();
const authApp = require('../middleware/authMiddleware');
const imgFilter = require('../middleware/imageFilter');
const multer = require('multer');
const upload = multer({ dest: 'public/tmp/', fileFilter: imgFilter.imageFIlter });
const uploadFile = upload.fields([{ name: 'image', maxCount: 1 }]);
const config = require('../config/config');
const productReq = require('../middleware/requests/productRequest');
const categoryReq = require('../middleware/requests/categoryRequest');
// controller
const productController = require('../controllers/productController');
const categoryController = require('../controllers/categoryController');
const tagController = require('../controllers/tagsController');
const cartController = require('../controllers/cartController');

router.post('/product', uploadFile, authApp('admin'), productReq.store, productController.store);
router.put('/product/:id', uploadFile, authApp('admin'),  productReq.update, productController.update);
router.get('/products', productController.index);
router.get('/product/:id', productController.detail);
router.delete('/product/:id', authApp('admin'), productController.destroy);
router.get('/category', authApp('all'), categoryController.index);
router.post('/category', authApp('admin'), categoryController.store);
router.get('/category/:id', categoryController.show);
router.put('/category/:id', categoryController.update);
router.delete('/category/:id', categoryController.destroy);
router.get('/tags', tagController.index);
router.post('/tag', authApp('admin'), tagController.store);
router.get('/tag/:id', authApp('admin'), tagController.show);
router.put('/tag/:id', authApp('admin'), tagController.update);
router.delete('/tag/:id', authApp('admin'), tagController.destroy);
router.put('/chart', authApp('member'), cartController.update);
router.get('/chart', authApp('member'), cartController.index);

router.get('/', (req, res) => {
    return res.render('index', { version: '1.0' });
});


module.exports = router;