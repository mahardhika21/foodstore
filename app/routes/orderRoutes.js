const express = require('express');
const router = express.Router();
const authApp = require('../middleware/authMiddleware');

const orderController = require('../controllers/orderController');

router.get('/order', authApp('member'), orderController.index);
router.post('/order', authApp('member'), orderController.store);
router.get('/invoie', authApp('member'), orderController.invoice);

module.exports = router;