const express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    upload = multer({ dest: 'public/tmp' }),
    authApp = require('../middleware/authMiddleware'),
    uploadData = upload.fields([{ name: 'avatar', maxCount: 1 }]);
const memberReq = require('../middleware/requests/loginRequest');
const authController = require('../controllers/loginController');
const deliverController = require('../controllers/deliveryController');

router.post('/login', memberReq.login, authController.login);
router.post('/member/register', memberReq.register, authController.register);
router.get('/test', authController.test);
router.get('/location', authController.location);
router.get('/deliver', authApp('member'), deliverController.index);
router.get('/deliver/:id', authApp('member'), deliverController.show);
router.post('/deliver', authApp('member'), deliverController.store);
router.put('/deliver/:id', authApp('member'), deliverController.update);
router.delete('/deliver/:id', authApp('member'), deliverController.destroy);


module.exports = router;