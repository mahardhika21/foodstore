async function findInarray(arr, search) {
    let status = false;
    for (const data of arr) {
        if (search === data) {
            status = true;
        }
    }

    return status;
}

async function sortArrProduct(type, mode) {
    if (type === 'price') {
        return {
            price: mode === 'asc' ? 'asc' : 'desc',
        };
    } else if (type === 'name') {
        return {
            name: mode === 'asc' ? 'asc' : 'desc',
        };
    } else if (type === 'sold') {
        return {
            sodl : mode == 'asc' ? 'asc' : 'desc',
        }   
   } else {
         return {
            created_at : mode === 'asc' ? 'asc' : 'desc',
        };
    }
}

async function queryArrProduct(type, value) {
    if (type == 'category') {
        return {
            active: 1,
            category_id: value
        };
    } else {
        return {
            active : 1
        }
    }
}

module.exports = {
    findInarray,
    sortArrProduct,
    queryArrProduct
}