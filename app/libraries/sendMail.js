const mailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require('../config/config');
var fs = require('fs');
const handlebars = require('handlebars');
const Logging = require('../libraries/logging');

async function main(data) {
   
    let initMailerTRN = await mailer.createTransport(smtpTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
            user:'nicktour21@gmail.com',
            pass: 'Layanantour2021'
        }
    }));
    
    let readHTMLFile = function(path, callback) {
        fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
            if (err) {
                throw err;
                callback(err);
            }
            else {
                callback(null, html);
            }
        });
    };

    readHTMLFile(`app/templates/mail/${data.view}.html`, function(err, html) {
        let template = handlebars.compile(html);
        
        let htmlToSend = template(data.content);
         let mailOptions = {
            from: data.from,
            to: data.to,
            subject: data.subject,
            html: htmlToSend
        };
        initMailerTRN.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });
    });
}

async function sendMail(data) {
    main(data);
}

module.exports = {
    sendMail
}