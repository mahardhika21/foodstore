const jwt = require("jsonwebtoken");
const config = require('../config/config');
const os = require('os');

module.exports = function (roles) {
  return async function (req, res, next) {
    const token = req.headers.authorization;

    if (!token) {
      return res.json({
        status: false,
        message: "Unauthorized Accesss, token not falid or empty"
      });
    }

    try {
      const decoded = jwt.verify(token, config.JWT_SECRET, {});
      if (decoded.hostname != os.hostname()) {
        return res.json({ success: false, message: 'user login failed' });
      }
      
      let cek = await authAccess(roles, decoded.type);
      if (cek == undefined) {
         return res.json({ success: false, message: 'user login not have access' });
      }

      return next();
    } catch (error) {
      return res.json({
        success: false,
        message: "Unauthorized Access"
      });
    }
  }
}

async function authAccess(roles, userType) {
    if (typeof(roles) === 'string') {
      if (roles == 'all') {
        return true;
      } else if (roles == userType) {
        return true;
      } else {
        return undefined;
      }
    } else {
        stat = roles.find((role) => {
          if (role == 'all') {
              return true
          };
          if (role == userType) {
            return true;
          }
        });
      return stat;
    }
}
