async function store(req, res, next) {
    
    if (!req.body.name) {
        return res.json({
            success: false,
            messgae: "name can'nt by null",
        });
    }

    if (!req.files.image) {
        return res.json({
            success: false,
            messgae: "picture can'nt by null",
        });
    }

    if (!req.body.price) {
        return res.json({
            success: false,
            message: 'price cannot by null',
        });
    }
    return next();
}

async function update(req, res, next) {
     if (!req.body.name) {
        return res.json({
            success: false,
            messgae: "name can'nt by null",
        });
    }
    return next();
}

module.exports = {
    store, update
}