async function login(req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.json({ success: false, message: 'email or password cannot empty' });
    }

    return next();
}

async function register(req, res, next) {
    if (!req.body.email || !req.body.password || !req.body.name) {
        return res.json({ success: false, message: 'email or password cannot empty' });
    }
    if (req.body.password.length < 6) {
        return res.json({ success: false, message: 'minimum passwor 6 caracter' });
    }

    return next();
}

module.exports = { login, register }