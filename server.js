let express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    config = require('./app/config/config'),
    morgan = require('morgan'),
    compression = require('compression'),
    path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose');
    app = express();
// const expressGraphQL = require('express-graphql').graphqlHTTP
// const graphqlSchema = require('./app/graphql/schema');
// const graphqlResolver = require('./app/graphql/resolvers');
const grapRouter = require('./app/graphql/router/index');

// console.log(Date.now());
let productRoutes = require('./app/routes/productRoutes');
let memberRoutes = require('./app/routes/memberRoutes');
let orderRouter = require('./app/routes/orderRoutes');

/* mongodb connectivity */

mongoose.Promise = global.Promise;
mongoose
    .connect(config.MONGODB_URI, {
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
        useUnifiedTopology : true,
    }).then(
        () => {
            console.log("Mongodb is connected");
        },
        err => {
            console.log("Cannot connect to the mongodb" + err);
        }
    );

// harus diatas pemanggilan cors dan body parser
// app.use('/graphql', expressGraphQL({
//     schema: graphqlSchema,
//     rootValue: graphqlResolver,
//     graphiql: true
// }));

// app.use('/graphql', grapRouter);
// app.use(grapRouter);

grapRouter

app.use(cors());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
// GraphQL schema


app.set('view engine', 'pug');
app.set("views", path.resolve("./app/views"));

app.use(compression());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(productRoutes);
app.use(memberRoutes);
app.use(orderRouter);
app.use(express.static(__dirname));


module.exports = app;
